# facelog

facelog 是一个用于人脸识别验证的跨平台跨语言的开发框架，其核心是一个基于 thrift 技术的 RPC 服务，为人脸识别应用提供数据管理、安全认证、前端设备管理、数据下发等基本核心的服务。

facelog 只是一个针对人脸识别应用的开发框架，并不针对特定的应用场景，应用项目在 facelog 的基础上根据facelog 提供的服务接口实现具体应用场景下的业务逻辑。

![系统结构图](https://i.imgur.com/S5lt7NO.png)

关于 facelog 更详细的说明参见 [《facelog 开发手册》](manual/MANUAL.md)




